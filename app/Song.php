<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    public function song_list() {
        return $this->belongsTo('App\SongList');
    }
}
