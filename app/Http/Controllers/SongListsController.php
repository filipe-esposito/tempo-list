<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SongList;
use Validator;

class SongListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $songlists = SongList::orderBy('date', 'desc')->get();
        return response()->json($songlists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'date' => 'required'
        ]);

        if($validator->fails()) {
            $response = array('response' => $validator->messages(), 'success' => false);
            return $response;
        } else {
            $songlist = new SongList;
            $songlist->name = $request->input('name');
            $songlist->date = $request->input('date');
            $songlist->save();

            return response()->json($songlist);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list = SongList::find($id);
        $list->songs = $list->songs;
        return response()->json($list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'date' => 'required'
        ]);

        if($validator->fails()) {
            $response = array('response' => $validator->messages(), 'success' => false);
            return $response;
        } else {
            $songlist = SongList::find($id);
            $songlist->name = $request->input('name');
            $songlist->date = $request->input('date');
            $songlist->save();

            return response()->json($songlist);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $list = SongList::find($id);
        $list->delete();
        $response = array('response' => 'Song list deleted', 'success' => true);
        return $response;
    }
}
