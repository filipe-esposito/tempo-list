<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Song;
use Validator;

class SongsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = Song::all();
        return response()->json($songs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'           => 'required',
            'bpm'            => 'required',
            'time_signature' => 'required',
            'song_list_id'   => 'required'
        ]);

        if($validator->fails()) {
            $response = array('response' => $validator->messages(), 'success' => false);
            return $response;
        } else {
            $song = new Song;
            $song->name           = $request->input('name');
            $song->bpm            = $request->input('bpm');
            $song->time_signature = $request->input('time_signature');
            $song->song_list_id   = $request->input('song_list_id');
            $song->save();

            return response()->json($song);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $song = Song::find($id);
        return response()->json($song);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'           => 'required',
            'song_list_id'   => 'required',
            'bpm'            => 'required',
            'time_signature' => 'required'
        ]);

        if($validator->fails()) {
            $response = array('response' => $validator->messages(), 'success' => false);
            return $response;
        } else {
            $song = Song::find($id);
            $song->song_list_id   = $request->input('song_list_id');
            $song->name           = $request->input('name');
            $song->bpm            = $request->input('bpm');
            $song->time_signature = $request->input('time_signature');
            $song->save();

            return response()->json($song);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $song = Song::find($id);
        $song->delete();
        $response = array('response' => 'Song list deleted', 'success' => true);
        return $response;
    }
}
